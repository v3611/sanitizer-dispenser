...well, this is embarrassing. This project is pretty much "done" but I've procrastinated on uploading the firmware sketch and can't find it anymore... Well, that's a lesson. Hope I'll find it soon. This project is still significant to me, as the layout B variant was actually my first PCB business card.

# Hand sanitizer dispenser

An automatic hand sanitizer dispenser with a capacitive proximity sensor.
Compared to optical sensing, it should be more immune to dirt, splatter, sunlight, skin tone variations, and can be incorporated into a plastic (non-conductive) enclosure without the need for adding a window or drilling additional holes.

![single board, raytraced](Sanitizer_dispenser_Rev.F3.jpg)

### In order to get it working you need to: 
-  Get the boards made and flash the microcontroller with a debug firmware (using a programmer like USBasp or an Arduino as ISP).
-  Add an antenna (a bent piece of thin isolated wire) - Read the appnotes to get an idea of what you're doing.
-  Experiment with the shape of your antenna and the sensitivity adjustment (Cs capacitor value) until your device works quite reliably.
-  Flash the microcontroller with a regular version of the firmware, which adds reliability improvements such as a forced recalibration timer.

**Don't bother unless you are capable of and willing to read through few pages of appnotes on capacitive proximity sensing, then experiment and validate your design. I've spent too many hours on remotely handholding someone who couldn't use his own multimeter.**

TL;DR Start small and increase size or sensitivity if needed. Don't make your antenna stupidly large, keep the antenna (active sensor area) itself and its preferably short wire away from ground planes, other wires, interference sources and floating metal.

### Interactive BOM
Layout A (61.5x41.5mm, 2-layer PCB): https://v3611.gitlab.io/sanitizer-dispenser/A-RevF3.html \
Layout B (85x55mm, 2nd layer as GND): https://v3611.gitlab.io/sanitizer-dispenser/B-RevA.html \
Generated using the awesome KiCad plugin: https://github.com/openscopeproject/InteractiveHtmlBom

### External links:
- [AT42QT1010 Datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/40001946A.pdf)
- [Secrets of a Successful QTouch(tm) Design](http://ww1.microchip.com/downloads/en/AppNotes/an-kd02_103-touch_secrets.pdf)
- [Microchip Capacitive Proximity Design Guide AN1492](http://ww1.microchip.com/downloads/en/Appnotes/01492A.pdf)
- [Capacitive Proximity Sensing Using the FDC1004 (different IC, but nicely written)](https://www.ti.com/lit/an/snoa928a/snoa928a.pdf)
